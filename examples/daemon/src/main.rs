// Copyright (c) 2020 by Enrique Pérez Arnaud <enrique at cazalla.net>    
//    
// This file is part of the modus_ponens project.    
// http://www.modus_ponens.net    
//    
// The modus_ponens project is free software: you can redistribute it and/or modify    
// it under the terms of the GNU General Public License as published by    
// the Free Software Foundation, either version 3 of the License, or    
// (at your option) any later version.    
//    
// The modus_ponens project is distributed in the hope that it will be useful,    
// but WITHOUT ANY WARRANTY; without even the implied warranty of    
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the    
// GNU General Public License for more details.    
//    
// You should have received a copy of the GNU General Public License    
// along with any part of the modus_ponens project.    
// If not, see <http://www.gnu.org/licenses/>.

extern crate modus_ponens;
#[macro_use]
extern crate modus_ponens_derive;

extern crate pest;
#[macro_use]
extern crate pest_derive;

use structopt::StructOpt;
//use std::{thread, time};

use log::info;
use std::mem;
use crate::modus_ponens::kbase::KBGen;
use crate::modus_ponens::kbase::KBase;
use std::io::{BufRead, BufReader, Write, BufWriter};
use std::os::unix::net::{UnixStream,UnixListener};

mod kb;


pub struct MPDaemon<'a> {
    kb: kb::KB<'a>,
    listener: UnixListener,
}

impl<'a> MPDaemon<'a> {
    pub fn new(socket: &'a str) -> Self {
        info!("Binding to {}...", socket);
        MPDaemon { 
            kb: kb::KBGenerator::gen_kb(),
            listener: UnixListener::bind(socket).unwrap(),
        }
    }
    fn handle_stream(self: &'a Self, stream: UnixStream) {
        let mut reader = BufReader::new(&stream);
        let mut writer = BufWriter::new(&stream);
        let mut buffer = String::new();
        match reader.read_line(&mut buffer) {
            Ok(_n) => {
                buffer.pop();
                info!("Received {}", &buffer);
                if buffer.ends_with("?") {
                    buffer.pop();
                    let resp = self.kb.ask(unsafe { mem::transmute( buffer.as_str() ) });
                    let resp = format!("{:?}", resp);
                    writer.write(resp.as_bytes()).unwrap();
                    writer.flush().unwrap();
                } else {
                    self.kb.tell(unsafe { mem::transmute( buffer.as_str() ) });
                }
            }
            Err(err) => {
                panic!("{}", err);
            }
        }
    }
    pub fn start(self: &'a Self) {
        for stream in self.listener.incoming() {
            match stream {
                Ok(stream) => {
                    self.handle_stream(stream);
                }
                Err(err) => {
                    info!("Error: {}", err);
                    break;
                }
            }
        }
    }
}


#[derive(Debug, StructOpt)]
#[structopt(name = "mpkb", about = "modus_ponens knowledge base")]
struct Opt {
    /// path of the unix socket to use.
    #[structopt(short, long)]
    socket: String,
}

fn main() {
    env_logger::init();
    let opt = Opt::from_args();
    let daemon = MPDaemon::new(&opt.socket);
    daemon.start();
}
