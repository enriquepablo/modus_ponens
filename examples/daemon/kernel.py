
import json
import os
import socket
import subprocess
import sys
import uuid

from traitlets import Dict
from metakernel import MetaKernel

from . import __version__


HELP_LINKS = [
    {
        'text': "modus_ponens",
        'url': "https://modus_ponens.cazalla.net",
    },
    {
        'text': "MP Kernel",
        'url': "https://mp_kernel.cazalla.net",
    },

] + MetaKernel.help_links


def get_kernel_json():
    """Get the kernel json for the kernel.
    """
    here = os.path.dirname(__file__)
    default_json_file = os.path.join(here, 'kernel.json')
    json_file = os.environ.get('MP_KERNEL_JSON', default_json_file)
    with open(json_file) as fid:
        data = json.load(fid)
    data['argv'][0] = sys.executable
    return data


class MPKernel(MetaKernel):
    app_name = 'mp_kernel'
    implementation = 'MP Kernel'
    implementation_version = __version__,
    language = 'modus_ponens'
    help_links = HELP_LINKS
    kernel_json = Dict(get_kernel_json()).tag(config=True)

    _mp_engine = None
    _socket = None
    _vars = {}
    language_version = '1.0.0'

    @property
    def language_info(self):
        return {'mimetype': 'text/x-mp',
                'name': 'modus_ponens',
                'file_extension': '.mp',
                'version': self.language_version,
                'help_links': HELP_LINKS}

    @property
    def banner(self):
        msg = 'MP Kernel v%s running modus_ponens v%s'
        return msg % (__version__, self.language_version)

    def set_variable(self, name, value):
        """
        Set a variable to a Python-typed value.
        """
        self._vars[name] = value

    def get_variable(self, name):
        """
        Lookup a variable name and return a Python-typed value.
        """
        return self._vars[name]

    def repr(self, item):
        """The repr of the kernel."""
        return repr(item)

    def get_usage(self):
        """Get the usage statement for the kernel."""
        return "This is a usage statement."

    def do_execute_direct(self, code, silent=False):
        """
        Execute code in the kernel language.
        """
        if code.endswith('?'):
            resp = self.mp_engine.ask(code.rstrip('?') + '<>')
            if not silent:
                self.Display(resp)
        else:
            self.mp_engine.tell(code)

    def restart_kernel(self):
        """Restart the kernel"""
        self._mp_engine = None

    @property
    def mp_engine(self):
        if self._mp_engine:
            return self._mp_engine
        self._socket = f"/tmp/mp-{str(uuid.uuid4())}.sock"
        self._mp_engine = MPEngine(self._socket)
        return self._mp_engine


class MPEngine:

    def __init__(self, socket):
        subprocess.run(["bin/mp", "--socket", socket])
        self.socket = socket

    def tell(self, knowledge):
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.connect(self.socket)
        knowledge += "\n"
        sock.sendall(knowledge.encode('utf8'))
        sock.close()

    def ask(self, question):
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sock.connect(self.socket)
        question += "?\n"
        sock.sendall(question.encode('utf8'))
        ans = sock.recv(255)
        sock.close()
        return ans
