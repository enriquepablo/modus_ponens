#!/bin/bash

for n in {1..20}
do

  for i in {1..601..20}
  do
    touch mopo/bench-mopo-$n.txt
    target/release/socrate-bench --facts $i --rules 50 --samples 1 --report 50 >> mopo/bench-mopo-$n.txt
  done;

done
