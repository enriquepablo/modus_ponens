#!/bin/bash

for n in {1..20}
do

  for i in {1..1000..20}
  do
    touch clips/bench-clips-$n.txt
    /home/eperez/venvs/clips/bin/python ./benchmark2_clips.py -n $i -r 50 -i 1 -s 50 >> clips/bench-clips-$n.txt
  done;

done
