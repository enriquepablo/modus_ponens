#!/bin/bash

for n in {1..20}
do

  for i in {1..601..20}
  do
    touch prolog/bench-prolog-$n.txt
    /home/eperez/venvs/prolog/bin/python ./benchmark_prolog.py -n $i -r 50 -i 1 -s 50 >> prolog/bench-prolog-$n.txt
  done;

done
